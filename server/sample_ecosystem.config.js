module.exports = {
  apps : [{
    name: 'server-main',
    script: 'lib/index.js',

    // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      PORT: 4000,
      SELECT_DB: "mongo", // mongo | mysql

      SQL_HOST: "localhost",
      SQL_USER: "<_xxxx_>",
      SQL_PASS: "<_xxxx_>",
      SQL_DB: "<_xxxx_>",
      SQL_PORT: "3306", // should be the default port

      MONGO_URL: "mongodb://localhost/",
      MONGO_DB: "<_xxxxx_>",

      AWS_ACCESS_KEY_ID: "<_xxxx_>",
      AWS_SECRET_KEY: "<_xxxx_>",
      AWS_BUCKET_NAME: "<_xxxx_>",
      AWS_REGION: "<_xxxx_>"// "eu-central-1"
    }
  }]
};
