import mysql from 'mysql';

const selectedDB = process.env.SELECT_DB;
let mySQLconnect: any = () => null;

if(selectedDB === 'mysql') {
    const mySQLconn = mysql.createConnection({
        host: process.env.SQL_HOST ,
        user: process.env.SQL_USER,
        port: parseInt(process.env.SQL_PORT!),
        password: process.env.SQL_PASS,
        database: process.env.SQL_DB,
        multipleStatements: true
    })

    mySQLconnect = (callback: () => void) => {
        mySQLconn.connect((err: Error) => {
            if(err) throw err;
    
            console.log('MySql successfully connected!')
            callback();
        })
    }
}

export default mySQLconnect; 