import mongoose from 'mongoose';

const selectedDB = process.env.SELECT_DB;
let mongoConnect: any = () => null;

if(selectedDB === 'mongo') {
    mongoConnect = (callback: () => void) => {
        mongoose.connect(`${process.env.MONGO_URL}${process.env.MONGO_DB}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        }).then(() => {
            console.log('MongoDb successfully connected!')
            callback();
        }).catch(err => {
            throw err;
        })
    }
}

export default mongoConnect;