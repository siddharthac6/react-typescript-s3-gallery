import express from 'express';
import cors from 'cors';
import * as bodyParser from 'body-parser';
import helmet from 'helmet';
import router from "./routes";
import mySQLconnect from "./config/db_mysql_config";
import mongoConnect from "./config/db_mongo_config";

const app = express();

app.use(helmet());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.disable('x-powered-by');
app.use('/', router);


const PORT = process.env.PORT;
const selectedDB = process.env.SELECT_DB;

const startServer = () => {
    app.listen(PORT || 1337, () => console.log(`main-server listening on port: ${PORT}`));
}

switch (selectedDB){
    case 'mongo':
        mongoConnect(startServer);
    case 'mysql':
        mySQLconnect(startServer);
    default: 
        console.log('No database selected');
}